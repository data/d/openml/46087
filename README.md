# OpenML dataset: Amazon_Prime_Fiction

https://www.openml.org/d/46087

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Description:**
The "amazon_prime_users.csv" dataset provides a structured overview of Amazon Prime users' profiles and their subscription details. It encompasses diverse attributes ranging from personal information to usage patterns, offering valuable insights into user behavior and preferences.

**Attribute Description:**
- **User ID**: A unique identifier for each user (e.g., 2026, 179).
- **Name**: The full name of the user (e.g., Jessica Flores, Tina Nelson).
- **Email Address**: User's email address (e.g., hallkendra@example.net).
- **Username**: A chosen username for the account (e.g., brandonlewis).
- **Date of Birth**: The birth date of the user (e.g., 1972-10-09).
- **Gender**: The gender of the user (Female/Male).
- **Location**: The user's location (e.g., Walterston, Murrayland).
- **Membership Start Date**: The date when the user's Prime membership started (e.g., 2024-02-07).
- **Membership End Date**: The expiration date of the current Prime membership (e.g., 2025-03-08).
- **Subscription Plan**: The type of Prime membership plan (Monthly/Annual).
- **Payment Information**: Type of payment card used (Mastercard/Visa).
- **Renewal Status**: Indicates if the membership will auto-renew or requires manual renewal.
- **Usage Frequency**: How often the user engages with Prime services (Regular/Occasional/Frequent).
- **Purchase History**: Types of products the user purchases (Clothing, Books, Electronics).
- **Favorite Genres**: Preferred content genres on Prime (Sci-Fi, Drama, Comedy).
- **Devices Used**: The devices used to access Prime services (Tablet, Smartphone).
- **Engagement Metrics**: Levels of engagement (Low/Medium/High).
- **Feedback/Ratings**: User-given ratings, on a scale of 1 to 5.
- **Customer Support Interactions**: Number of times the user has interacted with customer support.

**Use Case:**
This dataset can serve multiple purposes:
1. **Customer Segmentation**: Analyzing customer profiles to segment them based on preferences and usage patterns.
2. **Personalized Marketing**: Crafting tailored marketing campaigns or recommendations based on purchase history, favorite genres, and usage frequency.
3. **Membership Retention Strategies**: Identifying factors influencing renewal status to devise targeted retention strategies.
4. **Product Development**: Informing new features or services by understanding users' engagement and feedback/ratings.
5. **Customer Support Enhancement**: Enhancing support services by analyzing the frequency and nature of customer support interactions.
   
By exploring these aspects, businesses can derive actionable insights to enhance user experience, engagement, and loyalty in subscription-based services.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46087) of an [OpenML dataset](https://www.openml.org/d/46087). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46087/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46087/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46087/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

